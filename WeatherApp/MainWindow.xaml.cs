﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WeatherApp.ViewModel;

namespace WeatherApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainViewModel
            {
                NameOfDay = "Tuesday",
                Temperature = "22°",
                ApparentTemperature = "RealFeel: 31°",
                CityInformation = new Model.CityInformationModel(),
                CityInformationValues = new Model.CityInformationValuesModel(),
                ForecastSlots = new List<WeatherSlotViewModel>
                {
                    new WeatherSlotViewModel { NameOfDay = "Monday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(), BackgroundColor = Brushes.Wheat },
                    new WeatherSlotViewModel { NameOfDay = "Tuesday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(), BackgroundColor = Brushes.DarkRed },
                    new WeatherSlotViewModel { NameOfDay = "Wednesday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(), BackgroundColor = Brushes.Violet },
                    new WeatherSlotViewModel { NameOfDay = "Thursday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(), BackgroundColor = Brushes.Aquamarine },
                    new WeatherSlotViewModel { NameOfDay = "Friday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(), BackgroundColor = Brushes.Crimson },
                    new WeatherSlotViewModel { NameOfDay = "Saturday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(), BackgroundColor = Brushes.DarkGreen },
                    new WeatherSlotViewModel { NameOfDay = "Sunday", Temperature = "20°", ApparentTemperature = "RealFeel: 30°", CityInformation = new Model.CityInformationModel(), CityInformationValues = new Model.CityInformationValuesModel(),BackgroundColor = Brushes.LawnGreen }
                },
                BackgroundColor = Brushes.CadetBlue
            };

        }
    }
}
