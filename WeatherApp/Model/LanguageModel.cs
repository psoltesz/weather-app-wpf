﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.Model
{
    public class LanguageModel : ObservableCollection<string>
    {
        public LanguageModel()
        {
            Add("English");
            Add("Spanish");
            Add("Italian");
            Add("Hungarian");
            Add("French");
        }
    }
}
