﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.Model
{
    public class CityNamesModel : ObservableCollection<string>
    {
        public CityNamesModel()
        {
            Add("Budapest");
            Add("Luxembourg");
            Add("Debrecen");
            Add("Pécs");
            Add("Vienna");
            Add("Prague");
            Add("München");
            Add("Amsterdam");
        }
    }
}
