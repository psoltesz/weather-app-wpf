﻿using System.Collections.ObjectModel;

namespace WeatherApp.Model
{
    public class CityInformationModel : ObservableCollection<string>
    {
        public CityInformationModel()
        {
            Add("Atmospheric pressure:");
            Add("Wind speed:");
            Add("Humidity:");
            Add("UV index:");
        }
    }
}
