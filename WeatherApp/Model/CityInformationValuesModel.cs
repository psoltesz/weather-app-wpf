﻿using System.Collections.ObjectModel;

namespace WeatherApp.Model
{
    public class CityInformationValuesModel : ObservableCollection<string>
    {
        public CityInformationValuesModel()
        {
            Add("20");
            Add("11");
            Add("300");
            Add("2");
        }
    }
}
