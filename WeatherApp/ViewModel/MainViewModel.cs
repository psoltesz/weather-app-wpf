﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.ViewModel
{
    public class MainViewModel : WeatherSlotViewModel
    {
        private List<WeatherSlotViewModel> _forecastSlots;

        public List<WeatherSlotViewModel> ForecastSlots
        {
            get { return _forecastSlots; }
            set { SetValue(ref _forecastSlots, value); }
        }
    }
}
