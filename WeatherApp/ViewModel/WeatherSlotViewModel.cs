﻿using System.Windows.Media;
using WeatherApp.Model;

namespace WeatherApp.ViewModel
{
    public class WeatherSlotViewModel : ViewModelBase
    {
        private string _nameOfDay;
        private string _imagePath;
        private string _temperature;
        private string _apparentTemperature;
        private CityInformationModel _cityInformation;
        private CityInformationValuesModel _cityInformationValues;
        private Brush _backgroundColor;

        public string NameOfDay
        {
            get { return _nameOfDay; }
            set { SetValue(ref _nameOfDay, value); }
        }

        public string ImagePath
        {
            get { return "Resources/Images/rain.png"; }
            set { SetValue(ref _imagePath, value); }
        }

        public string Temperature
        {
            get { return _temperature; }
            set { SetValue(ref _temperature, value); }
        }

        public string ApparentTemperature
        {
            get { return _apparentTemperature; }
            set { SetValue(ref _apparentTemperature, value); }
        }

        public CityInformationModel CityInformation
        {
            get { return _cityInformation; }
            set { SetValue(ref _cityInformation, value); }
        }

        public CityInformationValuesModel CityInformationValues
        {
            get { return _cityInformationValues; }
            set { SetValue(ref _cityInformationValues, value); }
        }

        public Brush BackgroundColor
        {
            get { return _backgroundColor; }
            set { SetValue(ref _backgroundColor, value); }
        }
    }
}
